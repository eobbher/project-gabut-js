// #Deklarasi awal variabel

//load text pertanyaan
const elPertanyaan = document.getElementById('pertanyaan');
//load text pilihan ganda
const eljawaban_a = document.getElementById('pilihan_a');
const eljawaban_b = document.getElementById('pilihan_b');
const eljawaban_c = document.getElementById('pilihan_c');
const eljawaban_d = document.getElementById('pilihan_d');
//load pilihan ganda by id
const eljawaban_a_id = document.getElementById('a');
const eljawaban_b_id = document.getElementById('b');
const eljawaban_c_id = document.getElementById('c');
const eljawaban_d_id = document.getElementById('d');
//deklarasi tombol
const submitBtn = document.getElementById('submit');
//deklarasi list jawaban untuk disembunyikan ketika semua pertanyaan sudah terjawab
const listJawaban = document.getElementById('list-jawaban');

//deklarasi data pertanyaan dan jawaban
const kuisData = [
    {
        pertanyaan : 'Siapa Presiden Indonesia saat ini?',
        a: 'Jokowi',
        b: 'Amin',
        c: 'Anies',
        d: 'Prabowo',
        jawaban: 'a',
    },
    {
        pertanyaan : 'Apa warna bendera Indonesia?',
        a: 'Biru - Putih - Merah',
        b: 'Kuning - Putih',
        c: 'Merah - Putih',
        d: 'Hijau',
        jawaban: 'c',
    },
    {
        pertanyaan : 'Berapa lama presiden menjabat?',
        a: '30 th',
        b: '5 th',
        c: '3.5 th',
        d: '2 th',
        jawaban: 'b',
    },
    {
        pertanyaan : 'Ada berapa bait pancasila?',
        a: '6 bait',
        b: '4 bait',
        c: '5 bait',
        d: '7 bait',
        jawaban: 'c',
    },
    {
        pertanyaan : 'Apa hubungan status ku dan kamu?',
        a: 'Pacar Orang',
        b: 'Teman tapi suka ngga brani nyatain',
        c: 'Kang gojek',
        d: 'Bukan siapa-siapa. Benalu. cuih!',
        jawaban: 'd',
    },
];

//deklarasi variabel yang nilainya bisa berubah
let iKuisAktif = 0; //index soal yang aktif saat ini
let nilaiAkhir = 0; //nilai akhir dari jawaban benar
let jawabanAktif = ''; //variabel yang akan menyimpan jawaban tepat nomor saat ini

//fungsi untuk menampilkan pertanyaan di halaman html
function loadPertanyaan() {
    //mengambil array pertanyaan, pilihan dan jawaban
    const kuisDataAktif = kuisData[iKuisAktif];
    //menyematkan pertanyaan dan pilihan pada html menggunakan variabel constanta yang sudah di deklarasikan di awal
    elPertanyaan.innerText = kuisDataAktif.pertanyaan;
    eljawaban_a.innerText = kuisDataAktif.a;
    eljawaban_b.innerText = kuisDataAktif.b;
    eljawaban_c.innerText = kuisDataAktif.c;
    eljawaban_d.innerText = kuisDataAktif.d;
    //menyimpan jawaban dari pertanyaan yang sedang aktif saat ini
    jawabanAktif = kuisDataAktif.jawaban;
}

//fungsi untuk melakukan pengecekan jawaban yang diisikan
function cekJawaban() {
    //deklarasi variabel temporari untuk menyimpan jawaban pilihan user (dari radio button)
    let tempJawaban='';
    //melakukan pengecekan radio button mana yang dipilih user.
    if(eljawaban_a_id.checked){
        tempJawaban = 'a';
    }else if(eljawaban_b_id.checked){
        tempJawaban = 'b';
    }else if(eljawaban_c_id.checked){
        tempJawaban = 'c';
    }else if(eljawaban_d_id.checked){
        tempJawaban = 'd';
    }
    //melakukan pengecekan nilai hasil inputan user dengan jawaban benar yang sudah disimpan sebelumnya
    if (jawabanAktif == tempJawaban) {
        //apabila jawaban user sesuai, maka nilai akhir +1
        nilaiAkhir++;
    }
}
//pemanggilan fungsi awal untuk menampilkan soal pertama
loadPertanyaan();
//memberikan aksi pada button ketika button di klik oleh user 
submitBtn.addEventListener('click', function (){
    //menambah +1 pada index soal aktif saat ini, agar bisa berpindah ke soal berikutnya
    iKuisAktif++;
    //melakukan pengecekan index soal agar tidak melebihi panjang array data soal
    if (iKuisAktif < kuisData.length) {
        //memanggil fungsi pengecekan jawaban
        cekJawaban(); 
        //me-reset radio button pilihan user sebelumnya
        eljawaban_a_id.checked = false;
        eljawaban_b_id.checked = false;
        eljawaban_c_id.checked = false;
        eljawaban_d_id.checked = false;
        //memanggil fungsi pengambilan soal selanjutnya
        loadPertanyaan(); 
    }else{
        //memanggil fungsi pengecekan jawaban
        cekJawaban();
        //reset variabel jawaban agar ketika klik tombol tidak menambah nilai akhir
        jawabanAktif = '';
        //menyembunyikan list pilihan ganda
        listJawaban.style.display='none';
        //menampilkan nilai akhir dan total soal
        elPertanyaan.innerText = 'Nilai akhir anda ialah : '+nilaiAkhir+"/"+kuisData.length;
        //mengubah text pada button
        submitBtn.innerText = "Selesai";
    }
});
 