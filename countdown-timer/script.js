const elDetik = document.getElementById('detik');
const elMenit = document.getElementById('menit');
const elJam = document.getElementById('jam');
const elHari = document.getElementById('hari');

const tahunBaru = "1 Jan 2021";

function countdown() {
    const tglTahunBaru = new Date(tahunBaru);
    const tglHariIni = new Date();

    const rawDetik = (tglTahunBaru - tglHariIni) / 1000;
    const detik = Math.floor(rawDetik) % 60;
    const menit = Math.floor(rawDetik/60) % 60;
    const jam = Math.floor(rawDetik/3600) % 24;
    const hari = Math.floor(rawDetik/3600/24);


    elDetik.innerHTML = formatTime(detik);
    elMenit.innerHTML = formatTime(menit);
    elJam.innerHTML = formatTime(jam);
    elHari.innerHTML = formatTime(hari);
}

function formatTime(time) {
    return time < 10 ? `0${time}` : time;
}

countdown();

setInterval(countdown, 1000);